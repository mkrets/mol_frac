A simple tool to calculate H2 and CO fractions in the interstellar medium of galaxies.
Written in C++ with openMP.
Interoperability with python by using pybind11.

To install simply execute:
```bash
pip install .
```

Make sure to set the number of openMP threads:
```bash
export OMP_NUM_THREADS=16
```
In a notebook or jupyterlab use instead:
```python
%env OMP_NUM_THREADS=16
```

After installation simply import the module:
```python
import mol_frac
output = mol_frac.compute(all_mach_nr, all_nh, all_Z, all_turb, all_dx, G_o=1.0, T_mean=10.0)
X_H2, X_Co, L_CO = output[:,0], output[:,1], output[:,2]
```
where the outputs are the mean molecular fractions of H2 and CO and the line luminosity density (or simply the emissivity) of CO.

The arguments for `compute` are:
```
(all_mach: numpy.ndarray[float64], all_nh: numpy.ndarray[float64], all_Z: numpy.ndarray[float64], all_turb: numpy.ndarray[float64], all_dx: numpy.ndarray[float64], G_o: float, T_mean: float)
```
and returns a 2D numpy.ndarray with shape (xxx, 3) where the 3 columns are: X_H2, X_Co, L_CO.

Inputs and outputs are all in cgs units.

To convert the CO-emissivity to from erg s^{-1} cm^{-3} to K km s^{-1} pc^2 cm^{-3} multiply L_CO by this factor:
```
6.691044613526808e-29
```
The CO-luminosity for each cell can be computed by multiplying the emissivity with the cellsize^3.