#include <iostream>
#include <math.h>
#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

extern const double K_b = 1.380622e-16; // cgs
extern const double m_p = 1.672621e-24; // cgs
extern const double G = 6.6732e-8;      // cgs
extern const double c_cgs = 2.9979245e10;
extern const double h_ev = 4.135667e-15;
extern const double eV_to_ergs = 1.60218e-12;

struct Molecule
{
    double mu;
    double nu;
    double x_0;
    double x_1;
    double A_10;
    double B_10;
    double B_01;
};

void make_pdf(std::vector<double> &pdf, std::vector<double> s, double s_bar, double sigma_s)
{
    for (uint j = 0; j < s.size(); j++)
    {
        pdf[j] = 1. / sqrt(2. * M_PI * sigma_s * sigma_s) *
                 exp(-0.5 * (pow((s[j] - s_bar) / sigma_s, 2)));
    }
}

void calc_lambda_jeans(std::vector<double> &lambda_jeans, std::vector<double> n_H, double T_mean)
{
    for (uint i = 0; i < n_H.size(); i++)
    {
        lambda_jeans[i] = sqrt(K_b * T_mean / m_p) / sqrt(4. * M_PI * G * n_H[i] * m_p);
    }
}

void calc_n_LW(std::vector<double> &n_LW, std::vector<double> n_H, double G_o, std::vector<double> lambda_jeans, double Z)
{
    double kappa;

    kappa = 1000 * m_p * Z;

    for (uint i = 0; i < n_H.size(); i++)
    {
        n_LW[i] = G_o * exp(-kappa * n_H[i] * lambda_jeans[i]);
    }
}

void calc_X_H2(std::vector<double> &X_H2, std::vector<double> n_H, double Z, std::vector<double> n_LW)
{
    double DC = 1.7e-11;
    double CC = 2.5e-17;
    double numerator, denominator;
    for (uint i = 0; i < n_H.size(); i++)
    {
        numerator = DC * n_LW[i];
        denominator = CC * Z * n_H[i];
        X_H2[i] = 2. / (2. + (numerator / denominator));
    }
}

void calc_n_LW_ss(std::vector<double> &n_LW_ss, std::vector<double> n_H, std::vector<double> n_H2, double Z,
                  double G_o, std::vector<double> lambda_jeans)
{
    double kappa, exp_tau, N_H2, term1, term2, S_H2;
    for (uint i = 0; i < n_H.size(); i++)
    {
        kappa = 1000 * m_p * Z;
        exp_tau = exp(-kappa * n_H[i] * lambda_jeans[i]);
        N_H2 = n_H2[i] * lambda_jeans[i];
        term1 = 0.965 / pow(1.0 + (N_H2 / 5e14), 2);
        term2 = ((0.035 / sqrt(1 + (N_H2 / 5e14))) *
                 exp(-1 * sqrt(1 + (N_H2 / 5e14)) / 1180));
        S_H2 = term1 + term2;
        n_LW_ss[i] = G_o * exp_tau * S_H2;
    }
}

void self_shielding_iterations(std::vector<double> &n_LW, std::vector<double> &n_H2, std::vector<double> &X_H2_ss,
                               std::vector<double> n_H, double G_o, std::vector<double> lambda_jeans, double Z)
{
    int ctr = 16;
    std::vector<double> X_H2, n_LW_ss, n_H2_ss;

    X_H2.resize(n_H.size());
    n_LW_ss.resize(n_H.size());
    n_H2_ss.resize(n_H.size());

    calc_n_LW(n_LW, n_H, G_o, lambda_jeans, Z);
    calc_X_H2(X_H2, n_H, Z, n_LW);
    for (uint i = 0; i < n_H.size(); i++)
    {
        n_H2[i] = n_H[i] * X_H2[i];
        n_H2_ss[i] = n_H2[i];
    }
    for (int j = 0; j < ctr; j++)
    {
        calc_n_LW_ss(n_LW_ss, n_H, n_H2_ss, Z, G_o, lambda_jeans);
        calc_X_H2(X_H2_ss, n_H, Z, n_LW_ss);
        for (uint i = 0; i < n_H.size(); i++)
        {
            n_H2_ss[i] = n_H[i] * X_H2_ss[i];
        }
    }
}

double calc_integral(std::vector<double> s, std::vector<double> pdf, std::vector<double> X, double ds)
{
    double integral = 0;
    for (uint i = 0; i < s.size(); i++)
    {
        integral += exp(s[i]) * pdf[i] * X[i] * ds;
    }
    return integral;
}

void calc_X_CO(std::vector<double> &X_CO, std::vector<double> n_H, std::vector<double> n_H2, std::vector<double> n_LW)
{
    double x0 = 2.0e-4;
    double k0 = 5.0e-16;
    double k1 = 5.0e-10;
    double rate_CHX, rate_CO, factor_beta, beta, factor_CO;

    for (uint i = 0; i < n_H.size(); i++)
    {
        rate_CHX = 5.0e-10 * n_LW[i];
        rate_CO = 1.0e-10 * n_LW[i];

        factor_beta = rate_CHX / (n_H[i] * k1 * x0);
        beta = 1. / (1. + factor_beta);
        factor_CO = rate_CO / (n_H2[i] * k0 * beta);
        X_CO[i] = 1. / (1. + factor_CO);
    }
}

void calc_n_CO(std::vector<double> &n_CO, std::vector<double> n_H, std::vector<double> X_CO, double Z)
{
    double abundance_Ctot = 1e-4;
    for (uint i = 0; i < n_H.size(); i++)
    {
        n_CO[i] = n_H[i] * abundance_Ctot * Z * X_CO[i];
    }
}

void tau_LVG(std::vector<double> &tau, std::vector<double> n_CO, std::vector<double> lambda_jeans,
             std::vector<double> velocity, double cell_width, Molecule mol)
{
    double delta_nu;
    for (size_t i = 0; i < n_CO.size(); i++)
    {
        delta_nu = velocity[i] * mol.nu / c_cgs;
        tau[i] = h_ev * mol.nu * lambda_jeans[i] * n_CO[i] * ((mol.x_0 * mol.B_01) - (mol.x_1 * mol.B_10)) / (4.0 * M_PI * delta_nu);
    }
}

void beta_LVG(std::vector<double> &beta, std::vector<double> tau)
{
    for (size_t i = 0; i < tau.size(); i++)
    {
        if (tau[i] < 0.01)
        {
            beta[i] = 1.0 - tau[i] / 2.0;
        }
        else if (tau[i] > 100.)
        {
            beta[i] = 1. / tau[i];
        }
        else
        {
            beta[i] = (1.0 - exp(-tau[i])) / tau[i];
        }
    }
}

void calc_integrated_emissivity(std::vector<double> &j_10, std::vector<double> n_CO, Molecule mol)
{
    for (size_t i = 0; i < n_CO.size(); i++)
    {
        j_10[i] = n_CO[i] * h_ev * mol.nu * mol.x_1 * mol.A_10;
    }
}

void inside_loop(double &X_H2_bar, double &X_CO_bar, double &l_CO_bar,
                 double mach_no, double n_H_mean,
                 double Z, double sigma_turb, double cell_width,
                 double G_o, double T_mean, Molecule mol)
{
    const int SIZE = 100;

    std::vector<double> s(SIZE);
    std::vector<double> n_H(SIZE);
    std::vector<double> pdf(SIZE);
    std::vector<double> lambda_jeans(SIZE);
    std::vector<double> n_LW(SIZE);
    std::vector<double> n_H2(SIZE);
    std::vector<double> X_H2_ss(SIZE);
    std::vector<double> X_CO(SIZE);
    std::vector<double> n_CO(SIZE);
    std::vector<double> tau_nu(SIZE);
    std::vector<double> beta_nu(SIZE);
    std::vector<double> j_10(SIZE);
    std::vector<double> velocity(SIZE);
    std::vector<double> l_CO(SIZE);

    double sigma_s, s_bar, smin, smax;
    double ds;
    double c_s_CO, vel_disp;

    sigma_s = sqrt(log(1 + pow(0.3 * mach_no, 2)));
    s_bar = -0.5 * pow(sigma_s, 2);
    smin = -7.0 * sigma_s + s_bar;
    smax = 7.0 * sigma_s + s_bar;
    ds = (smax - smin) / SIZE;

    for (int i = 0; i < SIZE; i++)
    {
        s[i] = smin + i * ds;
        n_H[i] = n_H_mean * exp(s[i]);
    }

    make_pdf(pdf, s, s_bar, sigma_s);
    calc_lambda_jeans(lambda_jeans, n_H, T_mean);
    self_shielding_iterations(n_LW, n_H2, X_H2_ss, n_H, G_o, lambda_jeans, Z);

    X_H2_bar = calc_integral(s, pdf, X_H2_ss, ds);
    calc_X_CO(X_CO, n_H, n_H2, n_LW);
    calc_n_CO(n_CO, n_H, X_CO, Z);
    X_CO_bar = calc_integral(s, pdf, X_CO, ds);

    c_s_CO = sqrt(K_b * T_mean / (m_p * mol.mu));

    for (int i = 0; i < SIZE; i++)
    {
        vel_disp = sigma_turb * sqrt(lambda_jeans[i] / cell_width);
        velocity[i] = sqrt(pow(vel_disp, 2) + pow(c_s_CO, 2));
    }

    tau_LVG(tau_nu, n_CO, lambda_jeans, velocity, cell_width, mol);
    beta_LVG(beta_nu, tau_nu);
    calc_integrated_emissivity(j_10, n_CO, mol);

    for (int i = 0; i < SIZE; i++)
    {
        l_CO[i] = j_10[i] * beta_nu[i] * eV_to_ergs;
    }

    for (int i = 0; i < SIZE; i++)
    {
        l_CO_bar += l_CO[i] * pdf[i] * ds;
    }

    // py::print(l_CO_bar);
}

py::array py_compute(py::array_t<double, py::array::c_style | py::array::forcecast> all_mach,
                     py::array_t<double, py::array::c_style | py::array::forcecast> all_nh,
                     py::array_t<double, py::array::c_style | py::array::forcecast> all_Z,
                     py::array_t<double, py::array::c_style | py::array::forcecast> all_sigma_turb,
                     py::array_t<double, py::array::c_style | py::array::forcecast> all_cell_width,
                     double G_o, double T_mean)
{
    const ssize_t LENGTH = all_mach.size();

    // direct access
    auto all_mach_vec = all_mach.unchecked<1>();
    auto all_nh_vec = all_nh.unchecked<1>();
    auto all_Z_vec = all_Z.unchecked<1>();
    auto all_sigma_turb_vec = all_sigma_turb.unchecked<1>();
    auto all_cell_width_vec = all_cell_width.unchecked<1>();

    // initialize output vector
    ssize_t ndim = 2;
    ssize_t nr_output_var = 3;
    std::vector<double> result(nr_output_var * LENGTH);

    // call pure C++ function
    Molecule mol; // hard coded for CO here

    mol.mu = 28.0;
    mol.nu = 115271201800.0;
    mol.x_0 = 0.2520174421256517;
    mol.x_1 = 0.43479505590767203;
    mol.A_10 = 7.203e-08;
    mol.B_01 = 1.5329854426611306e-05;
    mol.B_10 = 5.1099514755371015e-06;

    ssize_t i;
#pragma omp parallel for
    for (i = 0; i < LENGTH; i++)
    {
        inside_loop(result[i * nr_output_var + 0], result[i * nr_output_var + 1], result[i * nr_output_var + 2],
                    all_mach_vec[i], all_nh_vec[i], all_Z_vec[i],
                    all_sigma_turb_vec[i], all_cell_width_vec[i],
                    G_o, T_mean, mol);
    }

    // reshape output into 2-D
    std::vector<ssize_t> shape = {LENGTH, nr_output_var};
    std::vector<ssize_t> strides = {(ssize_t)sizeof(double) * nr_output_var, sizeof(double)};

    // return 2-D NumPy array
    return py::array(py::buffer_info(
        result.data(),                           /* data as contiguous array  */
        sizeof(double),                          /* size of one scalar        */
        py::format_descriptor<double>::format(), /* data type                 */
        ndim,                                    /* number of dimensions      */
        shape,                                   /* shape of the matrix       */
        strides                                  /* strides for each axis     */
        ));
}

// wrap as Python module
PYBIND11_MODULE(mol_frac, m)
{
    m.doc() = "Calculate H2 and CO fractions.";
    m.def("compute", &py_compute,
          py::arg("all_mach"),
          py::arg("all_nh"),
          py::arg("all_Z"),
          py::arg("all_turb"),
          py::arg("all_dx"),
          py::arg("G_o"),
          py::arg("T_mean"),
          "Calculate H2 and CO fractions.");
}
