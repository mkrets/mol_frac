from setuptools import setup, Extension, find_packages

cc_options = ['--std=c++11', '-O3', '-fopenmp', '-Wall', '-Wextra', '-Wfatal-errors']

cc_extensions = [
    Extension(
        'mol_frac',
        sources=['src/cpp/main.cpp'],
        include_dirs=['src/cpp'],
        extra_compile_args=cc_options,
        extra_link_args=['-lgomp'],
        ),
    ]

setup(
    name='mol_frac',
    version='0.2.0',
    ext_modules=cc_extensions,
    install_requires = ['pybind11>2.4']
)
